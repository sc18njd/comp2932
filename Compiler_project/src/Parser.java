import java.io.IOException;
import java.util.ArrayList;

public class Parser {

    private final ArrayList<SymbolTable> MySymbolTable = new ArrayList<SymbolTable>(); //list of symbol tables for each class passed to compiler
    private final ArrayList<String> uncheckedTypes = new ArrayList<String>(); //list of undeclared types that are to be checked at end of compilation
    private final ArrayList<Function> uncheckedFunctions = new ArrayList<Function>(); //list of undeclared functions that are to be checked at end of compilation
    private final ArrayList<Variable> uncheckedVariables = new ArrayList<>(); //list of undeclared variables that are to be checked at end of compilation
    private final Variable calledVar = new Variable("", "", ""); //temp variable to hold the current variable within
    private Lexer MyLexer;
    private VMWriter myWriter;
    private SymbolTable tempSymbolTable; //temporary symbol table used for the current class being compiled
    private String LHStype = "", RHStype = ""; // variables to store the RHS and LHS type in an expression
    private String currFunctionType = ""; //temporary string variable to save the current functions type
    private int argsCounter = 0; //counter to track number of arguments passed in a call of a function
    private Function calledFunction = new Function("", ""); //temporary object to save a called function in
    private String tempClassName = "", tempFuncName = "";
    private boolean isArray = false, isRHS = false, isWhile = false, isIf = false, isExpression = false, isRelational = false, isArgument = false;
    //boolean variables to check for certain instances such as if the current symbol is within an if statement or is an Arrays index
    int labelCount = 0, loopLabelCount = 0;
    private ArrayList<String> labelStack = new ArrayList<String>();
    private ArrayList<String> loopLabelStack = new ArrayList<String>();


    //initialisation function used instead of the constructor function
    public void Init(String file_name) {
        MyLexer = new Lexer();

        if (!MyLexer.init(file_name)) {
            System.out.println("Unable to initialise the Lexer");
            System.exit(1);
        }

        System.out.println("Parser Initialised");

    }

    //function to print out a specified error message when error in file is found
    private void Error(MyToken t, String msg) {
        System.out.printf("Error in line %d at or near %s, %s\n", MyLexer.lineNo, t.Lexeme, msg);
        System.exit(1);
    }

    //function to print out warnings from their code
    private void Warning(MyToken t, String msg) {
        System.out.printf("Warning: in line %d at %s, %s\n", MyLexer.lineNo, t.Lexeme, msg);
    }

    //function to write out semantic errors that have occurred to screen
    private void SemanticError(MyToken t, String msg) {
        System.out.printf("Semantic Error in line %d %s\n", MyLexer.lineNo, msg);
    }

    //function to print when a token is accepted
    private void Accepted(MyToken t) {
        System.out.printf("symbol : %s , type : %s = ACCEPTED, \n", t.Lexeme, t.Type);
    }

    //function to print when semantics have been accepted within a line
    private void AcceptedSemantics() {
        System.out.printf("Line no. %d, Semantics accepted \n", MyLexer.lineNo);
    }

    //function to check if a given variable which was called from another class exists within that classes scope
    //returns true if so and false otherwise
    private boolean checkVariableExists(Variable temp) {
        //check the class called exists to begin with
        int classNumber = checkClassExists(temp.getClassName());

        //checking if where classname would ussually be that the type of the identifier used is actually of type of a class
        if (classNumber == -1) {
            if (checkVarIsTypeClass(temp.getClassName())) {
                classNumber = checkClassExists(calledFunction.getClassName());
            } else {
                return false;
            }
        }

        //checking the variable does exist
        if (!MySymbolTable.get(classNumber).variableAlreadyExists(temp.getVarName())) {
            return false;
        }

        //returns true or false depending if that variable was a field as only a field can be called from another class
        return MySymbolTable.get(classNumber).getKind(temp.getVarName()).equals("field");
    }

    //function to check if all unchecked variable types, variables and functions have declarations, this will be called at the end of compilation
    public void checkUnchecked() {
        //checking through list of unchecked variable types
        for (int i = uncheckedTypes.size() - 1; i >= 0; i--) {
            //if type matches an existing class name then it is removed from the list
            if (checkClassExists(uncheckedTypes.get(i)) != -1) {
                uncheckedTypes.remove(i);
            }
        }

        //checking through list of called functions which didn't have a declaration when called in code
        for (int i = uncheckedFunctions.size() - 1; i >= 0; i--) {
            if (checkClassExists(uncheckedFunctions.get(i).getClassName()) != -1) {
                SymbolTable temp = MySymbolTable.get(checkClassExists(uncheckedFunctions.get(i).getClassName()));

                if (checkFunctionExists(checkClassExists(uncheckedFunctions.get(i).getClassName()), uncheckedFunctions.get(i).getFunctionName()) != -1) {
                    int funcNum = checkFunctionExists(checkClassExists(uncheckedFunctions.get(i).getClassName()), uncheckedFunctions.get(i).getFunctionName());

                    //if class name and function name and finally all arguments passed are correct then item is removed from list
                    if (checkArguments(uncheckedFunctions.get(i), temp, funcNum)) {
                        uncheckedFunctions.remove(i);
                    }
                }
            }
        }

        //checking through list of called variables that didn't have an existing declaration in another class`
        for (int i = uncheckedVariables.size() - 1; i >= 0; i--) {
            //if variable exists within called class then removed from the list
            if (checkVariableExists(uncheckedVariables.get(i))) {
                uncheckedVariables.remove(i);
            }
        }
    }

    //function to check if the list of undeclared types is empty
    public boolean checkUncheckedTypeEmpty() {
        return this.uncheckedTypes.isEmpty();
    }

    //function to check if the list of called functions without declaration is empty
    public boolean checkUncheckFuncEmpty() {
        return this.uncheckedFunctions.isEmpty();
    }

    //function to check if the list of undeclared variables is empty
    public boolean checkUncheckedVarEmpty() {
        return this.uncheckedVariables.isEmpty();
    }

    //function to print the remaining functions/types/variables that were called and undeclared
    public void printUnchecked() {
        //check made that each isn't empty before attempting to print anything
        if (!checkUncheckFuncEmpty()) {
            for (int i = 0; i < uncheckedFunctions.size(); i++) {
                System.out.println("The following function was called and not declared in the same format: " + uncheckedFunctions.get(i).getClassName() + "." + uncheckedFunctions.get(i).getFunctionName());
            }
        }

        if (!checkUncheckedTypeEmpty()) {
            for (int i = 0; i < uncheckedTypes.size(); i++) {
                System.out.println("Type: '" + uncheckedTypes.get(i) + "' was attempted to be used but no class declaration of this type was compiled.");
            }
        }

        if (!checkUncheckedVarEmpty()) {
            for (int i = 0; i < uncheckedVariables.size(); i++) {
                System.out.println("Variable '" + uncheckedVariables.get(i).getVarName() + "' from class '" + uncheckedVariables.get(i).getClassName() + "' was attempted to be used without declaration");
            }
        }
    }


    //method to get the label at the top of the label stack
    private String getNextLabel(){
        String temp = labelStack.get(labelStack.size()-1);

        labelStack.remove(labelStack.size()-1);

        return temp;
    }

    //function to get the correct label if an if else statement has been used
    private String getNextLabelIfElse(){
        String temp = labelStack.get(labelStack.size()-2);

        labelStack.remove(labelStack.size()-2);

        return temp;
    }

    //function to add new label to the stack
    private void addNewLabel(String label){
        labelStack.add(label);
        labelCount++;
    }

    //method to get the label at the top of the loop label stack
    private String getNextLoopLabel(){
        String temp = loopLabelStack.get(labelStack.size()-1);

        loopLabelStack.remove(labelStack.size()-1);

        return temp;
    }

    //adding new loop label to the stack
    private void addNewLoopLabel(String label){
        loopLabelStack.add(label);
        loopLabelCount++;
    }

    //function to check if the variable passed to the function has been initialised by checking the symbol table
    private boolean checkRHSInit(String name) {
        //function is called a lot of the time without being needed to be checked so removing those calls without causing warnings
        if (!isRHS && !isIf && !isWhile) {
            return true;
        }

        //if the variable is of kind argument it will be initialised but this isn't saved in the symbol table
        if (tempSymbolTable.getKind(name).equals("argument")) {
            return true;
        } else if (tempSymbolTable.variableAlreadyExists(name)) {
            //when a single variable is used in an if statement, that doesn't necessarily have to be initialised
            if (isIf && !isRelational) {
                return true;
            }
            //returns the symbols initialised variable
            return tempSymbolTable.getVariableInit(name);
        } else {
            //returns false if variable doesn't exist
            return false;
        }
    }

    //function to check if a variable type being called is a class name of a class that has already been compiled
    private boolean checkVariableIsClassName(String name) {
        boolean isClassName = false;

        for (int i = 0; i < MySymbolTable.size(); i++) {
            if (name.equals(MySymbolTable.get(i).getClassName())) {
                isClassName = true;
                break;
            }
        }

        return isClassName;
    }


    //function to set the variable whihc tracks the RHS of a statements type
    private void setRHStype(String type, MyToken t) {

        //making sure function argument types aren't used to set a type
        if (isArgument) {
            return;
        }

        //setting RHS type to be matching if the RHS is a relational statement (e.g. let i = (2<3))
        if (isRelational && LHStype.equals("boolean")) {
            RHStype = "boolean";
            return;
        }

        //checking the index of array's type is an int or a char otherwise is semantically incorrect
        if (isArray && type.equals("char")) {

            Warning(t, "Using char as Array index");
            return;
        } else if (isArray && !type.equals("int")) {
            SemanticError(t, "Cannot use the type " + type + " as an index for an Array.");
        } else if (isArray) {
            return;
        }


        if (RHStype.equals("") || RHStype.equals("Array")) {
            //if the saved type is empty or is an Array set type to the passed argument
            RHStype = type;
        } else if (type.equals("Array")) {
            //issuing a warning as array type is valid, but the current index has to be of right type
            Warning(t, "using Array make sure Array value has correct type");
        } else {
            //if one type does not match the current saved type then saves type unmatching which will not be the LHS type
            if (!RHStype.equals(type)) {
                RHStype = "UNMATCHING";
            }
        }
    }

    //function to check the passed parameters of a called function match the arguments of a declared function
    private boolean checkArguments(Function function, SymbolTable temp, int funcNum) {

        //checking the number of arguments called matches the number of arguments saved
        if (!temp.getSubScope().get(funcNum).getKind().equals("method") && function.getArgsCount() != temp.getSubScope().get(funcNum).getNoOfArguments()) {
            return false;
        }
        //methods have an extra argument with "this" which doesn't need to be passed so different check made
        if (temp.getSubScope().get(funcNum).getKind().equals("method") && function.getArgsCount() != temp.getSubScope().get(funcNum).getNoOfArguments() - 1) {
            return false;
        }

        //running through the list of arguments passed by the call to check they are of correct type
        for (int i = 0; i < function.getArgsCount(); i++) {

            //checking passed argument in corresponding spot types match, if one doesn't match then below if statements return false immediately
            if (!temp.getSubScope().get(funcNum).getKind().equals("method")) {
                if (!temp.getSubScope().get(funcNum).getArguments().get(i).getType().equals(function.getArgumentsTypeList().get(i)) && (!temp.getSubScope().get(funcNum).getArguments().get(i).getType().equals("Array") &&
                        !function.getArgumentsTypeList().get(i).equals("Array"))) {
                    return false;
                }
            } else {
                //if subroutine is of kind method then argument in corresponding position +1 of saved function need to be compared
                if (!temp.getSubScope().get(funcNum).getArguments().get(i + 1).getType().equals(function.getArgumentsTypeList().get(i))) {
                    return false;
                }
            }
        }

        return true;
    }


    //function to check a class given a passed name exists within the saved symbol table
    //returns the position in the list of symbol tables that the class with the name exists
    private int checkClassExists(String className) {
        int classNumber = 0;

        //running through list of symbol tables
        for (; classNumber < MySymbolTable.size(); classNumber++) {
            //when symbol table class name matches the passed name, the position in the list is returned
            if (MySymbolTable.get(classNumber).getClassName().equals(className)) {
                return classNumber;
            }
        }

        //if current class is the classname then returns int greater than size of the saved list
        if (tempSymbolTable.getClassName().equals(className)) {
            return MySymbolTable.size() + 1;
        }

        //if class doesn't exist at all -1 returned
        return -1;
    }

    //function to check if a given function name exists within a given class symbol table
    //returns the functions position in the saved list of functions in the given symbol table
    private int checkFunctionExists(int classNumber, String funcName) {

        SymbolTable temp;

        //setting the symbol table which needs to be used in a temp variable
        if (classNumber > MySymbolTable.size()) {
            temp = tempSymbolTable;
        } else {
            temp = MySymbolTable.get(classNumber);
        }

        //running through the list of functions saved in the symbol table
        for (int i = 0; i < temp.getSubScopeSize(); i++) {
            //returns the position in the list if names match
            if (temp.getSubScope().get(i).getName().equals(funcName)) {
                return i;
            }
        }

        //returns -1 if it doesn't exist in given class
        return -1;
    }

    //function to check that the symbol that is typically a class name when calling a var or function isn't an object of
    //the given class, returns true if it is and false otherwise
    private boolean checkVarIsTypeClass(String name) {
        return !tempSymbolTable.getType(name).equals("NONE");
    }

    //function that checks all aspects of a called function by using the above functions
    private void checkFunction(MyToken t) {

        //if function called and class name or function name are blank then the process isn't needed
        if (calledFunction.getFunctionName().equals("") || calledFunction.getClassName().equals("")) {
            return;
        }

        int classNumber = checkClassExists(calledFunction.getClassName()); //getting position in saved symbol tables where class is
        int funcNumber;

        //if the class doesn't exist yet checking that it isn't of case where an object is used to call a function rather than a class name
        if (classNumber == -1) {

            if (checkVarIsTypeClass(calledFunction.getClassName())) {
                //changing the saved classname to the actual class name rather than objects name and getting that classes position
                calledFunction.setClassName(tempSymbolTable.getType(calledFunction.className));
                classNumber = checkClassExists(calledFunction.getClassName());
            }
        }


        if (classNumber != -1 && classNumber > MySymbolTable.size()) {
            //if class is the current class
            funcNumber = checkFunctionExists(classNumber, calledFunction.functionName);

            //checking function exists in scope
            if (funcNumber != -1) {

                //checking arguments of called function
                if (checkArguments(calledFunction, tempSymbolTable, funcNumber)) {
                    AcceptedSemantics();
                } else {

                    SemanticError(t, "Incorrect Number of arguments passed to function");
                }
            } else {
                Warning(t, "function may not have been compiled yet");

                //saving function if its not already saved to check at end of compilation
                if (!uncheckedFunctions.contains(calledFunction)) {
                    uncheckedFunctions.add(calledFunction);
                }
            }
        } else if (classNumber != -1) {
            //if class is saved in symbol table
            funcNumber = checkFunctionExists(classNumber, calledFunction.getFunctionName());

            //checking function exists in scope
            if (funcNumber != -1) {

                //checking arguments of called function
                if (checkArguments(calledFunction, MySymbolTable.get(classNumber), funcNumber)) {
                    AcceptedSemantics();

                } else {
                    System.out.println(calledFunction.getArgsCount());
                    SemanticError(t, "Incorrect Number of arguments passed to function");
                }
            } else {
                SemanticError(t, "Function " + calledFunction.getClassName() + "." + calledFunction.getFunctionName() + " does not exist within class");
            }
        } else {

            //saving function if its not already saved to check at end of compilation
            if (!uncheckedFunctions.contains(calledFunction)) {
                uncheckedFunctions.add(calledFunction);
            }
            Warning(t, "Class may have not been compiled yet.");

        }

        //resetting the variable as it has either been saved to check later or fully checked
        calledFunction = new Function("", "");
    }

    //initial descent parser function
    public void jackProg() throws IOException {
        MyToken t = MyLexer.peekNextToken();

        if (t.Type != MyToken.TokenTypes.EOF) {
            classDeclar();
        }
    }

    /*
     *  The rest of the descent parser function, using if statements to check grammar
     *  is correct, code generation and semantic analysis also completed within these functions.
     *
     *  Within these functions, when a while loop is used, this is to check for cases when 1 to many
     *  of an instance can be used within the grammar
     */
    private void classDeclar() throws IOException {
        MyToken t = MyLexer.getNextToken();

        if (t.Lexeme.equals("class")) {
            Accepted(t);
            t = MyLexer.getNextToken();

            if (t.Type.equals(MyToken.TokenTypes.id)) {
                Accepted(t);
                tempSymbolTable = new SymbolTable(t.Lexeme);
                myWriter = new VMWriter(t.Lexeme+".vm");
                t = MyLexer.getNextToken();

                if (t.Lexeme.equals("{")) {
                    Accepted(t);
                    t = MyLexer.peekNextToken();
                    while ((t.Type != MyToken.TokenTypes.EOF) && !(t.Lexeme.equals("}"))) {
                        memberDeclar();
                        t = MyLexer.peekNextToken();
                    }
                    if (t.Type == MyToken.TokenTypes.EOF) {
                        Error(t, "Expected closing to the classes scope");
                    } else {
                        t = MyLexer.getNextToken();
                        Accepted(t);

                        t = MyLexer.peekNextToken();

                        if (t.Type != MyToken.TokenTypes.EOF) {
                            Error(t, "Unexpected words after closing of class declaration");
                        }
                        System.out.println("Saved class " + tempSymbolTable.getClassName());
                        MySymbolTable.add(tempSymbolTable); //saving the current class to the list of symbol tables
                    }
                } else {
                    Error(t, "Expected opening of class scope");
                }
            } else {
                Error(t, "Expected identifier of class");
            }
        } else {
            Error(t, "Expected class keyword");
        }
    }

    private void memberDeclar() throws IOException {
        MyToken t = MyLexer.peekNextToken();
        if ((t.Lexeme.equals("static")) || (t.Lexeme.equals("field"))) {
            classVarDeclar();
        } else if ((t.Lexeme.equals("constructor")) || (t.Lexeme.equals("function"))
                || (t.Lexeme.equals("method"))) {
            subroutineDeclar();
        } else {
            Error(t, "Expected class or subroutine declarations");
        }
    }

    private void type() throws IOException {
        MyToken t = MyLexer.getNextToken();
        if (t.Lexeme.equals("int")) {
            Accepted(t);
        } else if (t.Lexeme.equals("char")) {
            Accepted(t);
        } else if (t.Lexeme.equals("boolean")) {
            Accepted(t);
        } else if (t.Type == MyToken.TokenTypes.id) {
            Accepted(t);
        } else if (t.Lexeme.equals(tempSymbolTable.getClassName())) {
            Accepted(t);
        } else if (checkVariableIsClassName(t.Lexeme)) {
            Accepted(t);
        } else {
            uncheckedTypes.add(t.Lexeme);
            Warning(t, "Possible non-type used, will check at end of compilation");
        }

        tempSymbolTable.setTempType(t.Lexeme); // setting the current variables type
    }

    private void classVarDeclar() throws IOException {
        MyToken t = MyLexer.getNextToken();

        if ((t.Lexeme.equals("static")) || (t.Lexeme.equals("field"))) {
            Accepted(t);
            tempSymbolTable.setTempKind(t.Lexeme); //saving the current variables kind
            type();

            t = MyLexer.getNextToken();

            if (t.Type == MyToken.TokenTypes.id) {

                //checking variable doesn't already exist within the scope
                if (tempSymbolTable.variableAlreadyExists(t.Lexeme)) {
                    SemanticError(t, "Variable already exists within the current scope.");
                }

                Accepted(t);
                AcceptedSemantics();

                tempSymbolTable.setTempName(t.Lexeme); //setting the current variables name
                tempSymbolTable.Define(); //defining the variables Symbol within the current symbol table

                MyToken next = MyLexer.peekNextToken();

                if (next.Lexeme.equals(",")) {

                    //checking for additional declarations in same definition
                    while ((next.Lexeme.equals(",")) || (next.Type == MyToken.TokenTypes.id)) {
                        t = MyLexer.getNextToken();
                        Accepted(t);
                        next = MyLexer.peekNextToken();

                        if ((next.Type == MyToken.TokenTypes.id) && (t.Type == MyToken.TokenTypes.id)) {
                            Error(t, "Expected comma");
                        } else if ((next.Lexeme.equals(",")) && (t.Lexeme.equals(","))) {
                            Error(t, "Expected another identifier between commas");
                        }

                        if (t.Type == MyToken.TokenTypes.id) {

                            if (tempSymbolTable.variableAlreadyExists(t.Lexeme)) {
                                SemanticError(t, "Variable already exists within the current scope.");
                            }

                            tempSymbolTable.setTempName(t.Lexeme); //setting variable name
                            tempSymbolTable.Define(); //saving variable to the symbol table
                        }
                    }

                    t = MyLexer.getNextToken();

                    if (t.Lexeme.equals(";")) {
                        Accepted(t);
                    } else {
                        Error(t, "Expected ';' at end of statement");
                    }
                } else if (next.Lexeme.equals(";")) {
                    t = MyLexer.getNextToken();
                    Accepted(t);
                } else {
                    Error(t, "Expected ',' between identifiers or ';' to end statement");
                }
            }
        } else {
            Error(t, "Expected static or field");
        }
    }

    private void subroutineDeclar() throws IOException {
        MyToken t = MyLexer.getNextToken();


        if ((t.Lexeme.equals("function")) || (t.Lexeme.equals("method"))) {
            Accepted(t);
            tempSymbolTable.newSubroutine(t.Lexeme);
            t = MyLexer.peekNextToken();

            if (t.Lexeme.equals("void") || t.Lexeme.equals(tempSymbolTable.getClassName())) {
                t = MyLexer.getNextToken();
                Accepted(t);
            } else {
                type();
            }

            if(!t.Lexeme.equals(tempSymbolTable.getClassName())) {
                tempSymbolTable.setMethodType(t.Lexeme); //setting method type
                currFunctionType = t.Lexeme; //setting current function
            } else {
                tempSymbolTable.setMethodType(t.Lexeme);
                currFunctionType = t.Lexeme;
            }

            t = MyLexer.getNextToken();

            if (t.Type == MyToken.TokenTypes.id) {
                Accepted(t);

                tempSymbolTable.setMethodName(t.Lexeme);

                t = MyLexer.getNextToken();

                if (t.Lexeme.equals("(")) {
                    Accepted(t);
                    paramList();

                    t = MyLexer.getNextToken();

                    if (t.Lexeme.equals(")")) {
                        Accepted(t);
                        subroutineBody();
                    } else {
                        Error(t, "Expected ')' ");
                    }
                } else {
                    Error(t, "Expected '(' for parameters");
                }
            } else {
                Error(t, "expected identifier");
            }
        } else if ((t.Lexeme.equals("constructor"))) {
            Accepted(t);
            tempSymbolTable.newSubroutine(t.Lexeme);
            t = MyLexer.getNextToken();

            if (t.Type == MyToken.TokenTypes.id) {
                Accepted(t);

                tempSymbolTable.setMethodType(t.Lexeme);
                currFunctionType = t.Lexeme;

                myWriter.writePush("constant", tempSymbolTable.variableCount("field")+tempSymbolTable.variableCount("static"));
                myWriter.writeCall("alloc","Memory", 1);

                t = MyLexer.getNextToken();

                if (t.Lexeme.equals("new")) {
                    Accepted(t);
                    tempSymbolTable.setMethodName(t.Lexeme);
                    t = MyLexer.getNextToken();

                    if (t.Lexeme.equals("(")) {
                        Accepted(t);
                        paramList();
                        t = MyLexer.getNextToken();

                        if (t.Lexeme.equals(")")) {
                            Accepted(t);

                            subroutineBody();
                        } else {
                            Error(t, "Expected ')' ");
                        }
                    } else {
                        Error(t, "Expected '(' for parameters");
                    }
                } else {
                    Error(t, "Expected 'new' for constructor definition");
                }
            }
        } else {
            Error(t, "Expected keyword");
        }

        tempSymbolTable.saveSubroutine();
    }

    private void paramList() throws IOException {

        MyToken t = MyLexer.peekNextToken();

        if (!t.Lexeme.equals(")")) {
            type();
            t = MyLexer.getNextToken();

            if (t.Type == MyToken.TokenTypes.id) {
                Accepted(t);
                tempSymbolTable.setTempKind("argument");
                tempSymbolTable.setTempName(t.Lexeme);
                tempSymbolTable.Define();

                MyToken next = MyLexer.peekNextToken();

                if (next.Lexeme.equals(",")) {
                    t = MyLexer.getNextToken();
                    Accepted(t);
                    next = MyLexer.peekNextToken();

                    while ((next.Type != MyToken.TokenTypes.EOF) && !(next.Lexeme.equals(")"))) {
                        type();
                        t = MyLexer.getNextToken();

                        if (t.Type == MyToken.TokenTypes.id) {
                            Accepted(t);

                            tempSymbolTable.setTempName(t.Lexeme);
                            tempSymbolTable.Define();

                            t = MyLexer.peekNextToken();

                            if (t.Lexeme.equals(",")) {
                                t = MyLexer.getNextToken();
                                Accepted(t);
                            }
                        } else {
                            Error(t, "Expected identifier");
                        }

                        next = MyLexer.peekNextToken();
                    }
                }
            }
        }

    }

    private void subroutineBody() throws IOException {
        MyToken t = MyLexer.getNextToken();
        boolean varCheck = false;

        if (t.Lexeme.equals("{")) {
            Accepted(t);
            t = MyLexer.peekNextToken();
            while (!(t.Lexeme.equals("}"))) {
                if(!varCheck && !t.Lexeme.equals("var")){
                    myWriter.writeFunction(tempSymbolTable.getCurrFuncName(),tempSymbolTable.getClassName(),tempSymbolTable.variableCount("var"));
                    varCheck = true;
                }
                statement();
                t = MyLexer.peekNextToken();
            }

            t = MyLexer.getNextToken();

            if (t.Lexeme.equals("}")) {
                Accepted(t);
            } else {
                Error(t, "Expected '}' ");
            }
        }
    }

    private void statement() throws IOException {
        MyToken t = MyLexer.peekNextToken();
        RHStype = "";
        LHStype = "";

        if (t.Lexeme.equals("var")) {
            varDeclarStatement();
        } else if (t.Lexeme.equals("let")) {
            letStatement();
        } else if (t.Lexeme.equals("if")) {
            ifStatement();
        } else if (t.Lexeme.equals("while")) {
            whileStatement();
        } else if (t.Lexeme.equals("do")) {
            doStatement();
        } else if (t.Lexeme.equals("return")) {
            returnStatement();
        } else {
            Error(t, "Expected keyword for statement");
        }
    }

    private void varDeclarStatement() throws IOException {
        MyToken t = MyLexer.getNextToken();

        if (t.Lexeme.equals("var")) {
            Accepted(t);
            tempSymbolTable.setTempKind(t.Lexeme);
            type();
            t = MyLexer.getNextToken();

            if (t.Type == MyToken.TokenTypes.id) {

                if (tempSymbolTable.variableAlreadyExists(t.Lexeme)) {
                    SemanticError(t, "Variable already exists within the current scope.");
                }
                Accepted(t);
                AcceptedSemantics();

                tempSymbolTable.setTempName(t.Lexeme);
                tempSymbolTable.Define();

                t = MyLexer.peekNextToken();

                if (t.Lexeme.equals(",")) {
                    t = MyLexer.getNextToken();
                    Accepted(t);
                    MyToken next = MyLexer.peekNextToken();

                    while (!(next.Lexeme.equals(";"))) {
                        t = MyLexer.getNextToken();
                        if (t.Type == MyToken.TokenTypes.id) {

                            if (tempSymbolTable.variableAlreadyExists(t.Lexeme)) {
                                SemanticError(t, "Variable already exists within the current scope.");
                            }
                            Accepted(t);

                            tempSymbolTable.setTempName(t.Lexeme);
                            tempSymbolTable.Define();

                            t = MyLexer.peekNextToken();

                            if (t.Lexeme.equals(",")) {
                                t = MyLexer.getNextToken();
                                Accepted(t);
                            }
                        } else {
                            Error(t, "Expected identifier");
                        }
                        next = MyLexer.peekNextToken();
                    }
                }

                t = MyLexer.getNextToken();
                if (t.Lexeme.equals(";")) {
                    Accepted(t);
                } else {
                    Error(t, "Expected ';' ");
                }
            }
        } else {
            Error(t, "Expected keyword 'var' ");
        }
    }

    private void letStatement() throws IOException {
        MyToken t = MyLexer.getNextToken();
        String LHSvar ="";
        boolean arrayInit = false;

        if (t.Lexeme.equals("let")) {
            Accepted(t);
            t = MyLexer.getNextToken();
            if (t.Type == MyToken.TokenTypes.id) {
                LHSvar = t.Lexeme;

                if (!tempSymbolTable.variableAlreadyExists(t.Lexeme)) {
                    SemanticError(t, "Variable being assigned value does not exist.");
                }
                Accepted(t);
                AcceptedSemantics();
                LHStype = tempSymbolTable.getType(t.Lexeme);
                tempSymbolTable.setVariableInit(t.Lexeme);

                t = MyLexer.getNextToken();

                if (t.Lexeme.equals("[")) {
                    arrayInit = tempSymbolTable.getVariableInit(LHSvar);
                    isArray = true;
                    Accepted(t);
                    expression();
                    t = MyLexer.getNextToken();

                    if (t.Lexeme.equals("]")) {
                        Accepted(t);

                        myWriter.writePush(tempSymbolTable.getKind(LHSvar),tempSymbolTable.getIndex(LHSvar));
                        myWriter.writeArithmetic("add");

                        isArray = false;
                        t = MyLexer.getNextToken();
                    } else {
                        Error(t, "Expected ']' ");
                    }
                }

                if (t.Lexeme.equals("=")) {
                    isRHS = true;
                    Accepted(t);
                    expression();
                    t = MyLexer.getNextToken();

                    if (LHStype.equals(RHStype) || LHStype.equals("Array") || RHStype.equals("Array") ||
                            (LHStype.equals("char") && RHStype.equals("int")) || (LHStype.equals("int") && RHStype.equals("char"))) {
                        AcceptedSemantics();
                    } else {
                        System.out.println(LHStype + RHStype);
                        SemanticError(t, "unmatching types on LHS and RHS");
                    }


                    if (t.Lexeme.equals(";")) {
                        if(!LHStype.equals("Array") || !arrayInit) {
                            myWriter.writePop(tempSymbolTable.getKind(LHSvar), tempSymbolTable.getIndex(LHSvar));
                        }
                        else{
                            myWriter.writePop("pointer", 1);
                            myWriter.writePush("temp", 0);
                            myWriter.writePop("that", 0);
                        }
                        Accepted(t);
                        LHStype = "";
                        RHStype = "";
                        isRHS = false;
                    } else {
                        Error(t, "Expected end symbol ';' ");
                    }
                } else {
                    Error(t, "Expected '[' or '=' ");
                }
            } else {
                Error(t, "Expected an identifier");
            }
        } else {
            Error(t, "Expected 'let' keyword");
        }
    }

    private void ifStatement() throws IOException {
        MyToken t = MyLexer.getNextToken();

        if (t.Lexeme.equals("if")) {
            Accepted(t);
            t = MyLexer.getNextToken();
            if (t.Lexeme.equals("(")) {
                isIf = true;
                Accepted(t);
                expression();
                t = MyLexer.getNextToken();

                if (t.Lexeme.equals(")")) {
                    isIf = false;
                    Accepted(t);
                    t = MyLexer.getNextToken();
                    myWriter.writeIf("IF"+labelCount);
                    addNewLabel("IF"+labelCount);


                    if (t.Lexeme.equals("{")) {
                        Accepted(t);
                        t = MyLexer.peekNextToken();

                        if (!(t.Lexeme.equals("}"))) {
                            while (!(t.Lexeme.equals("}"))) {
                                statement();
                                t = MyLexer.peekNextToken();
                            }
                        }

                        if (t.Lexeme.equals("}")) {
                            t = MyLexer.getNextToken();
                            Accepted(t);
                            t = MyLexer.peekNextToken();


                            if (t.Lexeme.equals("else")) {
                                t = MyLexer.getNextToken();
                                Accepted(t);
                                t = MyLexer.getNextToken();
                                myWriter.writeGoto("ElSE"+labelCount);
                                addNewLabel("ELSE"+labelCount);
                                myWriter.writeLabel(getNextLabelIfElse());

                                if (t.Lexeme.equals("{")) {
                                    Accepted(t);
                                    t = MyLexer.peekNextToken();

                                    if (!(t.Lexeme.equals("}"))) {
                                        while (!(t.Lexeme.equals("}"))) {
                                            statement();
                                            t = MyLexer.peekNextToken();
                                        }
                                    }

                                    if (t.Lexeme.equals("}")) {
                                        t = MyLexer.getNextToken();
                                        Accepted(t);
                                        myWriter.writeLabel(getNextLabel());
                                    } else {
                                        Error(t, "Expected '}' ");
                                    }

                                } else {
                                    Error(t, "Expected '{' ");
                                }
                            } else{
                                myWriter.writeLabel(getNextLabel());
                            }
                        } else {
                            Error(t, "Expected '}' ");
                        }
                    } else {
                        Error(t, "Expected '{' ");
                    }
                } else {
                    Error(t, "Expected ')' ");
                }
            } else {
                Error(t, "Expected '(' ");
            }
        } else {
            Error(t, "Expected keyword 'if' ");
        }
    }

    private void whileStatement() throws IOException {
        MyToken t = MyLexer.getNextToken();

        if (t.Lexeme.equals("while")) {
            Accepted(t);
            t = MyLexer.getNextToken();
            myWriter.writeLabel("LOOP_EXP"+loopLabelCount);
            addNewLoopLabel("LOOP_EXP"+loopLabelCount);

            if (t.Lexeme.equals("(")) {
                isWhile = true;
                Accepted(t);
                expression();
                t = MyLexer.getNextToken();

                if (t.Lexeme.equals(")")) {
                    isWhile = false;
                    Accepted(t);
                    t = MyLexer.getNextToken();
                    myWriter.writeIf("LOOP"+labelCount);
                    addNewLabel("LOOP"+labelCount);

                    if (t.Lexeme.equals("{")) {
                        Accepted(t);
                        t = MyLexer.peekNextToken();

                        if (!(t.Lexeme.equals("}"))) {

                            while (!(t.Lexeme.equals("}"))) {
                                statement();
                                t = MyLexer.peekNextToken();
                            }
                        }
                    }

                    if (t.Lexeme.equals("}")) {
                        t = MyLexer.getNextToken();
                        Accepted(t);
                        myWriter.writeGoto(getNextLoopLabel());
                        myWriter.writeLabel(getNextLabel());
                    } else {
                        Error(t, "Expected '{' ");
                    }
                } else {
                    Error(t, "Expected ')' ");
                }
            } else {
                Error(t, "Expected '(' ");
            }
        } else {
            Error(t, "Expected while keyword");
        }
    }

    private void doStatement() throws IOException {
        MyToken t = MyLexer.getNextToken();

        if (t.Lexeme.equals("do")) {
            Accepted(t);
            subroutineCall();
            myWriter.writePop("temp",0);
            t = MyLexer.getNextToken();

            if (t.Lexeme.equals(";")) {
                Accepted(t);
            } else {
                Error(t, "Expected ';' ");
            }
        } else {
            Error(t, "Expected do");
        }
    }

    private void subroutineCall() throws IOException {
        MyToken t = MyLexer.getNextToken();

        if (t.Type == MyToken.TokenTypes.id) {
            Accepted(t);
            tempClassName = t.Lexeme;

            if(tempSymbolTable.variableAlreadyExists(t.Lexeme)){
                myWriter.writePush(tempSymbolTable.getKind(t.Lexeme),tempSymbolTable.getIndex(t.Lexeme));
            }

            t = MyLexer.getNextToken();

            if (t.Lexeme.equals(".")) {

                Accepted(t);
                t = MyLexer.getNextToken();

                if (t.Type == MyToken.TokenTypes.id) {
                    Accepted(t);

                    tempFuncName = t.Lexeme;

                    calledFunction.setClassName(tempClassName);
                    calledFunction.setFunctionName(tempFuncName);

                    tempFuncName = "";
                    tempClassName = "";

                    t = MyLexer.getNextToken();
                }
            } else {
                tempFuncName = tempClassName;
                tempClassName = tempSymbolTable.getClassName();

                calledFunction.setClassName(tempClassName);
                calledFunction.setFunctionName(tempFuncName);

                tempFuncName = "";
                tempClassName = "";
            }

            if (t.Lexeme.equals("(")) {
                argsCounter = 0;
                isArgument = true;
                Accepted(t);
                expressionList();
                t = MyLexer.getNextToken();

                if (t.Lexeme.equals(")")) {
                    Accepted(t);
                    isArgument = false;
                    calledFunction.setArgsCount(argsCounter);

                    if(checkVarIsTypeClass(calledFunction.getClassName())){
                        myWriter.writeCall(calledFunction.getFunctionName(),tempSymbolTable.getType(calledFunction.getClassName()),calledFunction.getArgsCount()+1);
                    }
                    else {
                        myWriter.writeCall(calledFunction.getFunctionName(), calledFunction.getClassName(), calledFunction.getArgsCount());
                    }

                    checkFunction(t);
                } else {
                    Error(t, "Expected ')'");
                }
            } else {
                Error(t, "Expected '(' ");
            }
        } else {
            Error(t, "Expected an identifier");
        }
    }

    private void expressionList() throws IOException {

        MyToken t = MyLexer.peekNextToken();
        if (!(t.Lexeme.equals(")"))) {
            expression();

            t = MyLexer.peekNextToken();

            while (t.Lexeme.equals(",")) {
                t = MyLexer.getNextToken();
                Accepted(t);
                expression();
                t = MyLexer.peekNextToken();
            }
        }

    }

    private void returnStatement() throws IOException {
        MyToken t = MyLexer.getNextToken();

        if (t.Lexeme.equals("return")) {
            Accepted(t);
            t = MyLexer.peekNextToken();

            if (!(t.Lexeme.equals(";"))) {
                expression();

                if (currFunctionType.equals(RHStype)) {
                    AcceptedSemantics();
                } else {
                    SemanticError(t, "Return statement value type does not match function");
                }
                myWriter.writeReturn();
                t = MyLexer.peekNextToken();

            } else {
                if (currFunctionType.equals("void")) {
                    AcceptedSemantics();
                    myWriter.writePush("constant",0);
                    myWriter.writeReturn();
                } else {
                    SemanticError(t, "Return statement value type does not match function");
                }
            }

            if (t.Lexeme.equals(";")) {
                Accepted(t);
                t = MyLexer.getNextToken();
            } else {
                Error(t, "Expected ';' ");
            }
        }
    }

    private void expression() throws IOException {
        relationalExpression();

        MyToken t = MyLexer.peekNextToken();

        while ((t.Lexeme.equals("&")) || (t.Lexeme.equals("|"))) {
            String symbol = t.Lexeme;
            t = MyLexer.getNextToken();
            Accepted(t);
            relationalExpression();
            t = MyLexer.peekNextToken();

            switch (symbol) {
                case "&":
                    myWriter.writeArithmetic("and");
                    break;
                case "|":
                    myWriter.writeArithmetic("or");
                    break;
            }
        }
    }

    private void relationalExpression() throws IOException {
        arithmeticExpression();

        MyToken t = MyLexer.peekNextToken();

        while ((t.Lexeme.equals("=")) || (t.Lexeme.equals(">")) || (t.Lexeme.equals("<"))) {
            String symbol = t.Lexeme;

            if (isExpression) {
                isRelational = true;
            }

            t = MyLexer.getNextToken();
            Accepted(t);
            arithmeticExpression();
            isRelational = false;
            t = MyLexer.peekNextToken();

            switch (symbol){
                case "=":
                    myWriter.writeArithmetic("eq");
                    break;
                case ">":
                    myWriter.writeArithmetic("gt");
                    break;
                case "<":
                    myWriter.writeArithmetic("lt");
                    break;
            }
        }
    }

    private void arithmeticExpression() throws IOException {
        term();

        MyToken t = MyLexer.peekNextToken();

        while ((t.Lexeme.equals("+")) || (t.Lexeme.equals("-"))) {
            String symbol = t.Lexeme;
            t = MyLexer.getNextToken();
            Accepted(t);
            term();
            t = MyLexer.peekNextToken();
            argsCounter--;

            switch (symbol) {
                case "+":
                    myWriter.writeArithmetic("add");
                    break;
                case "-":
                    myWriter.writeArithmetic("sub");
                    break;
            }
        }
    }

    private void term() throws IOException {

        factor();

        MyToken t = MyLexer.peekNextToken();

        while ((t.Lexeme.equals("*")) || (t.Lexeme.equals("/"))) {
            String symbol = t.Lexeme;
            t = MyLexer.getNextToken();
            Accepted(t);
            factor();
            t = MyLexer.peekNextToken();
            argsCounter--;

            switch (symbol) {
                case "*":
                    myWriter.writeArithmetic("mult");
                    break;
                case "/":
                    myWriter.writeArithmetic("div");
                    break;
            }
        }
    }

    private void factor() throws IOException {
        MyToken t = MyLexer.peekNextToken();

        if ((t.Lexeme.equals("~")) || (t.Lexeme.equals("-"))) {
            t = MyLexer.getNextToken();
            Accepted(t);

            if(t.Lexeme.equals("-")) {
                myWriter.writeArithmetic("neg");
            }
        }
        operand();
    }

    private void operand() throws IOException {
        MyToken t = MyLexer.getNextToken();

        if (t.Type == MyToken.TokenTypes.Number) {
            setRHStype("int", t);
            if (isArgument && !isArray) {
                calledFunction.addNewArgument(t.Lexeme, "int");
                argsCounter++;
            }
            Accepted(t);
            myWriter.writePush("constant",Integer.parseInt(t.Lexeme.trim()));
        } else if ((t.Type == MyToken.TokenTypes.id)) {
            Accepted(t);
            String className = t.Lexeme;

            MyToken next = MyLexer.peekNextToken();

            if (next.Lexeme.equals(".")) {
                t = MyLexer.getNextToken();
                Accepted(t);
                t = MyLexer.getNextToken();

                if (t.Type == MyToken.TokenTypes.id) {
                    tempFuncName = t.Lexeme;

                    if (className.equals("other")) {
                        className = tempSymbolTable.getClassName();
                    }
                    calledFunction.setClassName(className);
                    calledFunction.setFunctionName(tempFuncName);
                    //class exists

                    if (checkClassExists(className) != -1) {
                        //function exists

                        if (checkFunctionExists(checkClassExists(className), tempFuncName) != -1) {

                            if (checkClassExists(className) > MySymbolTable.size()) {
                                //in current class
                                setRHStype(tempSymbolTable.getSubScope().get(checkFunctionExists(checkClassExists(className), tempFuncName)).getType(), t);
                            } else {
                                //in other class
                                setRHStype(MySymbolTable.get(checkClassExists(className)).getSubScope().get(checkFunctionExists(checkClassExists(className), tempFuncName)).getType(), t);
                            }
                        } else {

                            //if in current class the function may not have been compiled yet
                            if (checkClassExists(className) > MySymbolTable.size()) {
                                Warning(t, "Function may not have been compiled yet");

                                if (checkVarIsTypeClass(calledFunction.getClassName())) {
                                    calledFunction.setClassName(tempSymbolTable.getType(calledFunction.getClassName()));
                                }

                                setRHStype(LHStype, t);
                            } else {
                                SemanticError(t, "Function called which does not exist in given class " + className);
                            }
                        }
                    } else {
                        if (checkVarIsTypeClass(calledFunction.getClassName())) {
                            calledFunction.setClassName(tempSymbolTable.getType(calledFunction.getClassName()));
                        }

                        Warning(t, "Class may not have been compiled yet");
                        setRHStype(LHStype, t);
                    }

                    Accepted(t);
                    t = MyLexer.peekNextToken();
                    next = t;

                    if (!next.Lexeme.equals("(")) {
                        calledVar.setVarName(calledFunction.getFunctionName());
                        calledVar.setClassName(calledFunction.getClassName());
                        if (!checkVariableExists(calledVar)) {

                            uncheckedVariables.add(calledVar);
                        }
                    }
                } else {
                    Error(t, "Expected an identifier");
                }
            }

            if (!next.Lexeme.equals("(") && !next.Lexeme.equals("[")) {

                if (isArgument && !isArray && !t.Lexeme.equals("int")) {
                    calledFunction.addNewArgument(t.Lexeme, tempSymbolTable.getType(t.Lexeme));
                    argsCounter++;
                }

                if ((isIf || isWhile) && !checkRHSInit(t.Lexeme)) {
                    SemanticError(t, "Boolean variable within statement has not been initialised");
                }
                setRHStype(tempSymbolTable.getType(t.Lexeme), t);


                if (!checkRHSInit(t.Lexeme)) {
                    SemanticError(t, t.Lexeme + " is used without being initialised");
                }

                myWriter.writePush(tempSymbolTable.getKind(t.Lexeme),tempSymbolTable.getIndex(t.Lexeme));
            }

            if (next.Lexeme.equals("(")) {

                if(tempSymbolTable.variableAlreadyExists(className)){
                    myWriter.writePush(tempSymbolTable.getKind(className),tempSymbolTable.getIndex(className));
                }

                if (tempFuncName.equals("")) {
                    tempFuncName = className;
                    tempClassName = tempSymbolTable.getClassName();

                    calledFunction.setClassName(tempClassName);
                    calledFunction.setFunctionName(tempFuncName);
                }
                isArgument = true;
                argsCounter = 0;

                t = MyLexer.getNextToken();

                Accepted(t);
                expressionList();

                t = MyLexer.getNextToken();

                if (t.Lexeme.equals(")")) {
                    isArgument = false;
                    calledFunction.setArgsCount(argsCounter);

                    if(checkVarIsTypeClass(calledFunction.getClassName())){
                        myWriter.writeCall(calledFunction.getFunctionName(),tempSymbolTable.getType(calledFunction.getClassName()),calledFunction.getArgsCount()+1);
                    }
                    else {
                        myWriter.writeCall(calledFunction.getFunctionName(), calledFunction.getClassName(), calledFunction.getArgsCount());
                    }

                    checkFunction(t);
                    Accepted(t);

                } else {
                    Error(t, "Expected ')'");
                }
            }
            if (next.Lexeme.equals("[")) {
                isArray = true;
                if (isArgument) {
                    calledFunction.addNewArgument(t.Lexeme, "Array");
                    argsCounter++;
                }

                t = MyLexer.getNextToken();
                Accepted(t);
                expression();
                t = MyLexer.getNextToken();

                if (t.Lexeme.equals("]")) {
                    isArray = false;
                    Accepted(t);

                    myWriter.writePush(tempSymbolTable.getKind(className),tempSymbolTable.getIndex(className));
                    myWriter.writeArithmetic("add");
                    myWriter.writePop("pointer",1);
                    myWriter.writePush("that", 0);

                    setRHStype("Array", t);
                } else {
                    Error(t, "Expected ']'");
                }
            }
        } else if (t.Lexeme.equals("(")) {
            Accepted(t);
            isExpression = true;
            expression();
            t = MyLexer.getNextToken();

            if (t.Lexeme.equals(")")) {
                Accepted(t);
                isExpression = false;
            } else {
                Error(t, "expected ')' ");
            }
        } else if (t.Type == MyToken.TokenTypes.string_literal) {
            Accepted(t);
            if (isArgument) {
                calledFunction.addNewArgument(t.Lexeme, "String");
                argsCounter++;
            }
            myWriter.writePush("constant",t.Lexeme.length());
            myWriter.writeCall("new","String",1);

            for(int i=0;i<t.Lexeme.length();i++){
                myWriter.writePush("constant",(int)t.Lexeme.charAt(i));
                myWriter.writeCall("append","String",2);
            }

        } else if (t.Lexeme.equals("true")) {
            setRHStype("boolean", t);
            if (isArgument) {
                calledFunction.addNewArgument("true", "boolean");
                argsCounter++;
            }
            Accepted(t);
            myWriter.writePush("constant", -1);

        } else if (t.Lexeme.equals("false")) {
            setRHStype("boolean", t);
            if (isArgument) {
                calledFunction.addNewArgument("false", "boolean");
                argsCounter++;
            }
            Accepted(t);
            myWriter.writePush("constant", -0);

        } else if (t.Lexeme.equals("null")) {
            setRHStype(LHStype, t);
            Accepted(t);
        } else if (t.Lexeme.equals("this")) {
            Accepted(t);
            if (isArgument) {
                calledFunction.addNewArgument("this", tempSymbolTable.getClassName());
                argsCounter++;
            }
            setRHStype(tempSymbolTable.getClassName(), t);
            myWriter.writePush("pointer", 0);
        } else {
            Error(t, "Expected some kind of operand");
        }
    }

    class Function {
        private final ArrayList<String> argumentsName = new ArrayList<String>();
        private final ArrayList<String> argumentsType = new ArrayList<String>();
        private String className;
        private String functionName;
        private int argsCount;

        public Function(String className, String functionName) {
            this.className = className;
            this.functionName = functionName;
            this.argsCount = 0;
        }

        //getter and setter mmethods for the class

        public String getClassName() {
            return this.className;
        }

        public void setClassName(String name) {
            this.className = name;
        }

        public String getFunctionName() {
            return functionName;
        }

        public void setFunctionName(String functionName) {
            this.functionName = functionName;
        }

        public int getArgsCount() {
            return argsCount;
        }

        public void setArgsCount(int argsCount) {
            this.argsCount = argsCount;
        }

        public void addNewArgument(String name, String type) {
            this.argumentsName.add(name);
            this.argumentsType.add(type);
        }

        public ArrayList getArgumentsNameList() {
            return this.argumentsName;
        }

        public ArrayList getArgumentsTypeList() {
            return this.argumentsType;
        }
    }


    class Variable {
        private String className;
        private String varName;

        public Variable(String className, String varName, String varType) {
            this.className = className;
            this.varName = varName;
        }

        //getter and setter methods for the class
        
        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getVarName() {
            return varName;
        }

        public void setVarName(String varName) {
            this.varName = varName;
        }
    }
}
