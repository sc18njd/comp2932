import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.util.ArrayList;
import java.util.List;

public class Lexer {
    private final List<String> keywords = new ArrayList<String>();
    public int lineNo;
    private FileReader fr;
    private PushbackReader pr;

    public Lexer() {
        //initilaising keywords list
        keywords.add("class");
        keywords.add("constructor");
        keywords.add("method");
        keywords.add("function");
        keywords.add("int");
        keywords.add("boolean");
        keywords.add("char");
        keywords.add("void");
        keywords.add("var");
        keywords.add("static");
        keywords.add("field");
        keywords.add("let");
        keywords.add("do");
        keywords.add("if");
        keywords.add("else");
        keywords.add("while");
        keywords.add("return");
        keywords.add("true");
        keywords.add("false");
        keywords.add("null");
        keywords.add("this");

    }

    //Initialising class
    public boolean init(String file_name) {
        //opening file and buffer
        if (file_name.contains(".jack")) {
            File f = new File(file_name);
            try {
                fr = new FileReader(f);
                pr = new PushbackReader(fr, 100000);
            } catch (IOException e) {
                System.out.println("No valid file input, could not open file!");
                return false;
            }
        } else {
            System.out.println("No valid file input");
            return false;
        }
        lineNo = 1;
        return true;
    }


    //Function used to get the next token
    public MyToken getNextToken() throws IOException {
        int c = peekNextChar();
        MyToken token = new MyToken();

        //passes by any newline characters
        while ((char) c == '\n') {
            pr.read();
            lineNo++;
            c = peekNextChar();

        }

        //skip whitespace
        while ((c != -1) && Character.isWhitespace((char) c)) {
            if (c == '\n') {
                lineNo++;
            }
            pr.read();
            c = peekNextChar();

        }


        //checks if comment then skips if so
        if ((char) c == '/') {
            pr.read();

            boolean comment = true;

            while (comment == true) {

                int next = peekNextChar();

                //checking for inline comments
                if ((char) next == '/') {
                    c = next;
                    while ((c != -1) && ((char) c != '\n')) {
                        pr.read();
                        c = peekNextChar();
                    }


                    //gets rid of possible lines between comment sections
                    while ((char) c == '\n' || Character.isWhitespace((char) c)) {
                        pr.read();
                        if (c == '\n') {
                            lineNo++;
                        }
                        c = peekNextChar();
                    }

                    //check if the next line is a potential comment as well
                    c = peekNextChar();

                    if ((char) c != '/') {
                        comment = false;
                    } else {
                        pr.read();
                    }


                    //checking for multiple line comments
                } else if ((char) next == '*') {
                    c = next;
                    pr.read();
                    next = peekNextChar();
                    while ((c != -1)) {
                        if (c == '\r') {
                            lineNo++;
                        }
                        pr.read();
                        c = next;
                        next = peekNextChar();
                        if (((char) c == '*') && ((char) next == '/')) {
                            break;
                        }
                    }
                    pr.read();

                    c = peekNextChar();


                    //gets rid of possible lines between comment sections
                    while ((char) c == '\n') {
                        pr.read();
                        lineNo++;


                        c = peekNextChar();
                    }

                    //check if the next line is a potential comment as well
                    c = peekNextChar();
                    if ((char) c != '/') {
                        comment = false;
                    }

                    //otherwise it isn't a comment and moves on
                } else {
                    comment = false;
                    pr.unread(c);
                }
            }
        }

        //skip whitespace
        while ((c != -1) && Character.isWhitespace((char) c)) {
            if (c == '\n') {
                lineNo++;
            }
            pr.read();
            c = peekNextChar();

        }

        //checking for end of file
        if (c == -1) {
            pr.read();
            token.Type = MyToken.TokenTypes.EOF;
            return token;
        }

        //checking for a string literal
        if ((char) c == '"') {
            pr.read();
            c = peekNextChar();
            while (c != -1 && (char) c != '"') {
                token.Lexeme += (char) c;
                pr.read();
                c = peekNextChar();
            }
            pr.read();
            c = peekNextChar();

            token.Type = MyToken.TokenTypes.string_literal;
            return token;
        }

        //checking for a keyword/identifier
        if (Character.isLetter((char) c) || ((char) c == '_')) {
            token.Lexeme += (char) c;
            pr.read();
            c = peekNextChar();
            while ((c != -1) && (Character.isLetter((char) c) || Character.isDigit((char) c)) || ((char) c == '_')) {
                token.Lexeme += (char) c;
                pr.read();
                c = peekNextChar();

            }

            //runs through list of keywords to check is keyword
            for (int i = 0; i < keywords.size(); i++) {
                if (token.Lexeme.equals(keywords.get(i))) {
                    token.Type = MyToken.TokenTypes.Keyword;
                    return token;
                }
            }

            token.Type = MyToken.TokenTypes.id;
            return token;

        }

        //check for integer numbers
        if (Character.isDigit((char) c)) {
            while ((c != -1) && (Character.isDigit((char) c))) {
                token.Lexeme += (char) c;
                pr.read();
                c = peekNextChar();
            }

            token.Type = MyToken.TokenTypes.Number;
            return token;
        }

        //otherwise it is symbol
        token.Lexeme += (char) c;
        pr.read();
        token.Type = MyToken.TokenTypes.Symbol;
        return token;
    }


    //Function to peek the next token
    public MyToken peekNextToken() throws IOException {
        //instantiates a mark, uses the getTokenFunction and then resets back to the mark
        MyToken token = getNextToken();
        if (token.Type == MyToken.TokenTypes.string_literal) {
            pr.unread('"');
            pr.unread(token.Lexeme.toCharArray());
            pr.unread('"');
        } else {
            pr.unread(token.Lexeme.toCharArray());
        }

        return token;
    }


    //Function to peek the next character
    private int peekNextChar() throws IOException {
        //reads from the buffer stores value to pass back and adds the value back to the buffer
        int temp = 0;
        temp = pr.read();
        pr.unread(temp);

        return temp;
    }


    //function to close the file after use
    public void CloseFile() throws IOException {
        pr.close();
        fr.close();
    }
}
