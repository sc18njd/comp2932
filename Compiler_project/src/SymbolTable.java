import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

public class SymbolTable {

    private final Hashtable<String, Symbol> classScope; //class scope symbol table
    private final Hashtable<String, Symbol> tempSubScope; //table for temporary scope for current function
    private final ArrayList<FunctionSymbol> subScope; //list of saved functions for current class
    private final String className; // class name of object
    private boolean inSubScope;
    private int staticIndex, fieldIndex, argIndex, varIndex;
    private String tempName;
    private String methodName;
    private String tempType;
    private String tempKind;
    private String methodType;
    private String methodKind;

    //constructor class
    public SymbolTable(String name) {
        className = name;
        classScope = new Hashtable<String, Symbol>();
        tempSubScope = new Hashtable<String, Symbol>();
        subScope = new ArrayList<FunctionSymbol>();
        inSubScope = false;
    }

    //getter method for the number of functions in the class
    public int getSubScopeSize() {
        return this.subScope.size();
    }

    //getter method for the list of the functions in the class
    public ArrayList<FunctionSymbol> getSubScope() {
        return this.subScope;
    }

    public String getCurrFuncName(){
        return methodName;
    }

    //saves the current functions name and number of arguments
    public void saveSubroutine() {
        FunctionSymbol temp = new FunctionSymbol(methodType, methodName, methodKind, variableCount("argument"));

        Enumeration<String> e = tempSubScope.keys();

        //running through the arguments in the symbol table to save them
        while (e != null && e.hasMoreElements()) {
            String key = e.nextElement();

            if (tempSubScope.get(key) != null && tempSubScope.get(key).getKind().equals("argument")) {
                temp.addArgument(tempSubScope.get(key));
            }
        }

        subScope.add(temp);
        System.out.printf("Saved method %s\n", methodName);
        methodType = "void"; //resetting variable after class saved
    }

    //setter method for current methods type
    public void setMethodType(String type) {
        methodType = type;
    }

    //setter method for current methods Name
    public void setMethodName(String name) {
        methodName = name;
    }

    //setter method for type of current variable
    public void setTempType(String type) {
        tempType = type;
    }

    //setter method for kind of current symbol
    public void setTempKind(String kind) {
        tempKind = kind;
    }

    //setter method for name of current symbol
    public void setTempName(String name) {
        tempName = name;
    }

    //method to empty the temporary function symbol table for the scope for a new function
    public void newSubroutine(String subKind) {
        tempSubScope.clear();
        inSubScope = true;
        methodKind = subKind;

        //for a method an initial argument is required of type class name
        if (subKind.equals("method")) {
            tempSubScope.put("this", new Symbol(className, "argument", 0));
            argIndex = 1;
        } else {
            argIndex = 0;
        }

        varIndex = 0;
    }

    //main function that will add a new addition to the correct scope symbol table
    public void Define() {
        int i = 0;

        //if static or field needs to be added to class scope
        if (tempKind.equals("static") || tempKind.equals("field")) {
            switch (tempKind) {
                case "static":
                    i = staticIndex++;
                    break;
                case "field":
                    i = fieldIndex++;
                    break;
            }
            System.out.printf("Added new symbol %s to class scope, type:%s, kind:%s, index:%d\n", tempName, tempType, tempKind, i);
            classScope.put(tempName, new Symbol(tempType, tempKind, i));
        }
        //otherwise is in a sub scope
        else if (tempKind.equals("argument") || tempKind.equals("var")) {
            switch (tempKind) {
                case "argument":
                    i = argIndex++;
                    break;
                case "var":
                    i = varIndex++;
                    break;
            }
            System.out.printf("Added new symbol %s to sub scope, type:%s, kind:%s, index:%d\n", tempName, tempType, tempKind, i);
            tempSubScope.put(tempName, new Symbol(tempType, tempKind, i));
        }
    }

    //getter class for the class name
    public String getClassName() {
        return this.className;
    }

    //getter method for the kind of a string which is passed to function
    public String getKind(String name) {
        if (inSubScope) {
            if (tempSubScope.get(name) != null) {
                return tempSubScope.get(name).getKind();
            }
        }
        if (classScope.get(name) != null)
            return classScope.get(name).getKind();
        return "NONE";
    }

    //getter method for the type of a string which is passed to function
    public String getType(String name) {

        if (tempSubScope.get(name) != null) {
            return tempSubScope.get(name).getType();
        }

        if (classScope.get(name) != null)
            return classScope.get(name).getType();

        return "NONE";
    }

    //function to check if a variable already exists in symbol table with a given name, will return true if already exists
    public boolean variableAlreadyExists(String name) {

        if (tempSubScope.get(name) != null) {
            return true;
        }

        return classScope.get(name) != null;
    }

    //getter method for the index of an element within the scope
    public int getIndex(String name) {
        if (inSubScope) {
            if (tempSubScope.get(name) != null) {
                return tempSubScope.get(name).getIndex();
            }
        }

        if (classScope.get(name) != null)
            return classScope.get(name).getIndex();

        return -1;
    }

    //getter method for the number of variables of a given kind
    public int variableCount(String kind) {

        int count = 0;

        Hashtable<String, Symbol> temp = null;

        //checks which scope we need to check
        if (kind.equals("var") || kind.equals("argument")) {
            temp = tempSubScope;
        } else if (kind.equals("field") || kind.equals("static")) {
            temp = classScope;
        }

        Enumeration<String> e = temp.keys();

        //runs through the hashtable increasing the count if the kind is found
        while (e != null && e.hasMoreElements()) {
            String key = e.nextElement();

            if (temp.get(key) != null && temp.get(key).getKind().equals(kind)) {
                count++;
            }
        }
        return count;
    }

    //setter method for the init variable of a symbol
    public void setVariableInit(String name) {
        if (tempSubScope.get(name) != null) {
            tempSubScope.get(name).setInitialised();
        } else {
            classScope.get(name).setInitialised();
        }
    }

    //getter method for the init variable of a symbol
    public boolean getVariableInit(String name) {
        if (tempSubScope.get(name) != null) {
            return tempSubScope.get(name).getInit();
        } else {
            return classScope.get(name).getInit();
        }
    }

    //sub class for a symbol
    class Symbol {
        private final String type;
        private final String kind;
        private final int index;
        private boolean init = false;

        public Symbol(String type, String kind, int index) {
            this.type = type;
            this.kind = kind;
            this.index = index;
            if(type.equals("char")){
                init = true;
            }
        }

        //getter and setter methods for the variables of the class
        public String getType() {
            return this.type;
        }

        public String getKind() {
            return this.kind;
        }

        public int getIndex() {
            return this.index;
        }

        public boolean getInit() {
            return this.init;
        }

        public void setInitialised() {
            init = true;
        }
    }

    //sub class for a function symbol
    class FunctionSymbol {
        private final int noOfArguments;
        private final String name;
        private final String type;
        private final String kind;
        private ArrayList<Symbol> arguments = new ArrayList<>(); //list of arguments for this function

        public FunctionSymbol(String type, String name, String kind, int arguments) {
            this.noOfArguments = arguments;
            this.name = name;
            this.type = type;
            this.kind = kind;
        }

        //getter and setter methods for classes variables
        public String getType() {
            return this.type;
        }

        public String getName() {
            return this.name;
        }

        public int getNoOfArguments() {
            return this.noOfArguments;
        }

        public String getKind() {
            return this.kind;
        }

        public void addArgument(Symbol argument){
            this.arguments.add(argument);
        }

        public ArrayList<Symbol> getArguments() {
            return arguments;
        }
    }

}
