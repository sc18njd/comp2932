import java.io.IOException;
import java.util.ArrayList;

public class
Main {
    public static void main(String[] args) throws IOException {
        //checking filenames were passed to compile
        if (args.length < 1) {
            System.out.println("Error: expected file names on compilation.");
            System.out.println("Format in the commandline should be 'java Main <filename> ... <filename>'");
            System.exit(1);
        }

        //adding the default jack libraries to list to be compiled
        ArrayList<String> libraries = new ArrayList<String>();
        libraries.add("lib/Math.jack");
        libraries.add("lib/Memory.jack");
        libraries.add("lib/Screen.jack");
        libraries.add("lib/Output.jack");
        libraries.add("lib/Keyboard.jack");
        libraries.add("lib/String.jack");
        libraries.add("lib/Array.jack");
        libraries.add("lib/Sys.jack");

        //adding passed arguments to list of files to be compiled
        for (int i = 0; i < args.length; i++) {
            libraries.add(args[i]);
        }
        Parser parser = new Parser();

        //parsing each of the files one by one
        for (int i = 0; i < libraries.size(); i++) {
            parser.Init(libraries.get(i));
            parser.jackProg();
        }

        //checking if any variables have been left unchecked
        parser.checkUnchecked();
        if (!parser.checkUncheckedVarEmpty() || !parser.checkUncheckFuncEmpty() || !parser.checkUncheckedTypeEmpty()) {
            //if any functions or variables are left after final check then an error is posted along with all the remaining variables/functions
            System.out.println("System could not run as the following problems occurred:");
            parser.printUnchecked();
            System.exit(1);
        }
    }
}
