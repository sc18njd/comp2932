import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class VMWriter {

    private File file;
    private PrintStream ps = null;

    //Constructor function
    public VMWriter(String filename){

        //creating new file in set directory
        file = new File("VM/"+filename);

        try{
            ps = new PrintStream(file);
        }
        catch(IOException e){
            System.out.println("Error attempting to open new file for VM writer");
            e.printStackTrace();
            System.exit(1);
        }
    }

    //method to write push command to file
    public void writePush(String segment, int index){
        if(segment.equals("field")){
            ps.println(String.format("push this %d", index));
        }
        else if(segment.equals("var")){
            ps.println(String.format("push local %d", index));
        }
        else {
            ps.println(String.format("push %s %d", segment, index));
        }
    }

    //method to write pop command to file
    public void writePop(String segment, int index){
        if(segment.equals("field")){
            ps.println(String.format("pop this %d", index));
        }
        else if(segment.equals("var")){
            ps.println(String.format("pop local %d", index));
        }
        else {
            ps.println(String.format("pop %s %d", segment, index));
        }
    }

    //method to write arithmetic commands to file
    public void writeArithmetic(String command){
        if(command.equals("mult")){
            ps.println("call Math.multiply 2");
        }
        else if(command.equals("div")){
            ps.println("call Math.divide 2");
        }
        else{
            ps.println(String.format("%s",command.toLowerCase()));
        }

    }

    //method to write label command to file
    public void writeLabel(String label){
        ps.println(String.format("label %s", label));
    }

    //method to write goto command to file
    public void writeGoto(String label){
        ps.println(String.format("goto %s",label));
    }

    //method to write if command to file
    public void writeIf(String label){
        ps.println("not");
        ps.println(String.format("if-goto %s",label));
    }

    //method to write function command to file
    public void writeFunction(String name, String className, int nLocals){
        ps.println(String.format("function %s.%s %d",className,name,nLocals));
    }

    //method to write call command to file
    public void writeCall(String name, String className, int nArgs){
        ps.println(String.format("call %s.%s %d",className,name,nArgs));
    }

    //method to write return command to file
    public void writeReturn(){
        ps.println("return");
    }

    //function to close the Print stream
    public void close(){
        ps.close();
    }

}
